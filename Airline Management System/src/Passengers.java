
import java.util.Date;


public class Passengers {
    private String Passenger_name;
    private String nationality;
  //  private Gender;
    private String Passport_no;
    private String Address;
    private int Phone;
    private Gender gender;
    

   
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
     public String getPassenger_name() {
        return Passenger_name;
    }


    public void setPassenger_name(String Passenger_name) {
        this.Passenger_name = Passenger_name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPassport_no() {
        return Passport_no;
    }

    public void setPassport_no(String Passport_no) {
        this.Passport_no = Passport_no;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public int getPhone() {
        return Phone;
    }

    public void setPhone(int Phone) {
        this.Phone = Phone;
    }
        
        
}
