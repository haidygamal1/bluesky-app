
public class Tickets {
    private String Passenger_id;
    private String Passenger_name;
    private String Flight_code;
   // private Gender;
    private String Passport_no;
    private double Amount;
    private String nationality;

    public String getPassenger_id() {
        return Passenger_id;
    }

    public void setPassenger_id(String Passenger_id) {
        this.Passenger_id = Passenger_id;
    }

    public String getPassenger_name() {
        return Passenger_name;
    }

    public void setPassenger_name(String Passenger_name) {
        this.Passenger_name = Passenger_name;
    }

    public String getFlight_code() {
        return Flight_code;
    }

    public void setFlight_code(String Flight_code) {
        this.Flight_code = Flight_code;
    }

    public String getPassport_no() {
        return Passport_no;
    }

    public void setPassport_no(String Passport_no) {
        this.Passport_no = Passport_no;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double Amount) {
        this.Amount = Amount;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
    
    
    
}
