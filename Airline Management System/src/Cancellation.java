
public class Cancellation {
    private String Ticket_id;
    private String Flight_code;
    private double fees;

    public String getTicket_id() {
        return Ticket_id;
    }

    public void setTicket_id(String Ticket_id) {
        this.Ticket_id = Ticket_id;
    }

    public String getFlight_code() {
        return Flight_code;
    }

    public void setFlight_code(String Flight_code) {
        this.Flight_code = Flight_code;
    }

    public double getFees() {
        return fees;
    }

    public void setFees(double fees) {
        this.fees = fees;
    }
    
    
}