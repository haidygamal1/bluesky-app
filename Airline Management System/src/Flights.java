
public class Flights {
    private String Flight_code;
    private String Source;
    private String Destination;
   // private date TakeOfDate;
    private int Number_of_Seats;

    public String getFlight_code() {
        return Flight_code;
    }

    public void setFlight_code(String Flight_code) {
        this.Flight_code = Flight_code;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String Source) {
        this.Source = Source;
    }

    public String getDestination() {
        return Destination;
    }

    public void setDestination(String Destination) {
        this.Destination = Destination;
    }

    public int getNumber_of_Seats() {
        return Number_of_Seats;
    }

    public void setNumber_of_Seats(int Number_of_Seats) {
        this.Number_of_Seats = Number_of_Seats;
    }
    
    
    
    
}

